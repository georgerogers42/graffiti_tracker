defmodule GraffitiTracker.Routes.ReportJSON do
  require EEx
  def init(req, state) do
    params = :cowboy_req.parse_qs(req) |> Enum.into(%{})
    lname = params["alderman"]
    aparams = Map.take(params, ["ward"])
    gparams = Map.take(params, ["ward", "street_address", "zip_code", "police_district", "$limit"])
    start_date = GraffitiTracker.DB.parse_datetime(params["start_date"])
    stop_date = GraffitiTracker.DB.parse_datetime(params["stop_date"], Timex.now)
    aldermen = GraffitiTracker.DB.aldermen(aparams).body |> Jason.decode! |> Enum.filter(&String.ends_with?(&1["alderman"], lname || ""))
    data = GraffitiTracker.DB.data(gparams, aparams, lname) |> Enum.filter(&Timex.between?(GraffitiTracker.DB.parse_datetime(&1["creation_date"]), start_date, stop_date))
    data = %{"count" => Enum.count(data), "wards" => aldermen |> Stream.map(&{&1["ward"], &1["alderman"]}) |> Enum.into(%{}), "start_date" => Timex.format!(start_date, "{ISO:Extended}"), "stop_date" => Timex.format!(stop_date, "{ISO:Extended}"), input: params}
    req = :cowboy_req.reply(200, %{"content-type" => "application/json"}, Jason.encode!(data) <> "\n", req)
    {:ok, req, state}
  end
end
