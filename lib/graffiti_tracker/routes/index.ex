defmodule GraffitiTracker.Routes.Index do
  require EEx
  def init(req, state) do
    req = :cowboy_req.reply(200, %{"content-type" => "text/html"}, template(), req)
    {:ok, req, state}
  end
  EEx.function_from_file :def, :template, "templates/index.eex", []
end
