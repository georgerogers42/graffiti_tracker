defmodule GraffitiTracker.Routes.Report do
  require EEx
  def init(req, state) do
    params = :cowboy_req.parse_qs(req) |> Enum.into(%{})
    lname = params["alderman"]
    aparams = Map.take(params, ["ward"])
    gparams = Map.take(params, ["ward", "street_address", "zip_code", "police_district", "$limit"])
    start_date = GraffitiTracker.DB.parse_datetime(params["start_date"])
    stop_date = GraffitiTracker.DB.parse_datetime(params["stop_date"], Timex.now)
    data = GraffitiTracker.DB.data(gparams, aparams, lname) |> Enum.filter(&Timex.between?(GraffitiTracker.DB.parse_datetime(&1["creation_date"]), start_date, stop_date))
    req = :cowboy_req.reply(200, %{"content-type" => "text/html"}, template(data, params, start_date, stop_date), req)
    {:ok, req, state}
  end
  EEx.function_from_file :def, :template, "templates/report.eex", [:data, :params, :start_date, :stop_date]
end
