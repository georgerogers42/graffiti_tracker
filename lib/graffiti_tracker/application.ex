defmodule GraffitiTracker.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications

  import Logger

  use Application

  @doc """
  Returns the  port to start on.
  """
  def port(d \\ 8080) do
    try do
      String.to_integer(System.get_env("PORT"))
    rescue
      ArgumentError ->
        d
    end
  end
  def app_token do 
    System.get_env("APP_TOKEN")
  end
  def routes do
    api_key = app_token()
    [
      _: [
        {"/", GraffitiTracker.Routes.Index, %{api_key: api_key}},
        {"/report", GraffitiTracker.Routes.Report, %{api_key: api_key}},
        {"/report.json", GraffitiTracker.Routes.ReportJSON, %{api_key: api_key}}
      ]
    ]
  end

  def start(_type, _args) do
    d = :cowboy_router.compile(routes())
    p = port()
    {:ok, s} = :cowboy.start_clear(:graffiti_tracker, [port: p], %{env: %{dispatch: d}})
    info(:io_lib.format("Starting on port: ~b", [p]))
    # List all child processes to be supervised
    children = [
      # Starts a worker by calling: GraffitiTracker.Worker.start_link(arg)
      # {GraffitiTracker.Worker, arg},
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: GraffitiTracker.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
