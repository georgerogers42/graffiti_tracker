defmodule GraffitiTracker.DB do
  @doc """
  Parsed date time according to ISO8601 defaulting to the start of the UNIX Epoch.
  """
  def parse_datetime(sd, d \\ Timex.epoch |> Timex.Timezone.convert(Timex.Timezone.get("America/Chicago")))
  def parse_datetime(sd, d) when is_binary(sd) do
    try do
      Timex.parse!(sd, "{ISO:Extended}")
    rescue
      Timex.Parse.ParseError ->
        d
    end
  end
  def parse_datetime(_sd, d) do
    d
  end

  def has_value({_k, v}) do
    v !== ""
  end
  defp do_merge(s, m) do
    s |> Stream.map(&Map.merge(&1, m))
  end
  defp merge_tab({k0, a}, {k1, b}) when k0 === k1 do
    a |> Stream.flat_map(&do_merge(b, &1))
  end
  defp merge_tab({k0, a}, {k1, b}) do
    []
  end
  defp merge_tabs(am, b) do
    Stream.flat_map(am, &merge_tab(b, &1))
  end
  def merge_by(a, b, k) do
    am = Enum.group_by(a, &(&1[k]))
    bm = Enum.group_by(b, &(&1[k]))
    Stream.flat_map(am, &merge_tabs(bm, &1)) |> Enum.sort_by(&(&1["creation_date"]), &(&2 < &1))
  end
  @doc """
  [Graffiti removal requests data set API description](https://dev.socrata.com/foundry/data.cityofchicago.org/cdmx-wzbz)
  """
  def graffiti(query \\ %{}, api_key \\ "") do
    query = Stream.filter(query, &has_value/1) |> Enum.into(%{})
    url = "https://data.cityofchicago.org/resource/cdmx-wzbz.json"
    HTTPotion.get(url, [query: query])
  end
  @doc """
  Returns the aldermen based on the query params for [Ward offices data set API description](https://dev.socrata.com/foundry/data.cityofchicago.org/7ia9-ayc2)
  """
  def aldermen(query \\ %{}, api_key \\ "") do
    query = Stream.filter(query, &has_value/1) |> Enum.into(%{})
    url = "https://data.cityofchicago.org/resource/htai-wnw4.json"
    HTTPotion.get(url, [query: query])
  end
  def data(gparams \\ %{}, aparams \\ %{}, lname \\ "")
  def data(gparams, aparams, lname) do
    aldermen = GraffitiTracker.DB.aldermen(aparams).body |> Jason.decode! |> Enum.filter(&String.ends_with?(&1["alderman"], lname || ""))
    aldermen |> Enum.flat_map(&GraffitiTracker.DB.graffiti(gparams |> Map.put("ward", &1["ward"])).body |> Jason.decode! |> GraffitiTracker.DB.merge_by(aldermen, "ward")) |> Enum.sort_by(&(&1["created_date"]))
  end
end
