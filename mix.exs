defmodule GraffitiTracker.MixProject do
  use Mix.Project

  def project do
    [
      app: :graffiti_tracker,
      version: "0.1.0",
      elixir: "~> 1.6",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      docs: [
        extras: ["README.md"]
      ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :timex, :jason, :inets, :httpotion, :cowboy],
      mod: {GraffitiTracker.Application, []},
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:jason, "~> 1.0"},
      {:cowboy, "~> 2.2.0"},
      {:httpotion, "~> 3.1.0"},
      {:timex, "~> 3.1"},
      {:ex_doc, "~> 0.16", only: :dev, runtime: false}
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"},
    ]
  end
end
