# GraffitiTracker

To start the server:

    $ mix deps.get
    $ PORT=5000 iex -S mix # Will Log the port it is listening on
    $ curl "127.0.0.1:5000/report.json?alderman=Garza"
    {"input":{"alderman":"Garza"},"count":1000,"start_date":"1969-12-31T18:00:00-06:00","stop_date":"2018-02-18T23:25:36.668227+00:00","wards":{"10":"Susan Sadlowski Garza"}}

## JSON Web API

### GET /report.json
URL Params

All params are defaulted to not filter on that param.

    start_date :: ISO 8601 Date Time
    stop_date :: ISO 8601 Date Time
    zip_code :: String
    police_district :: Integer # Police District Number
    ward :: Integer # Ward Number
    alderman :: String # The end of the Aldermans name
    $limit :: Integer # Number Of Rows to limit request per ward to

Result

JSON Object containing these fields:

    count :: Integer
    wards :: {ward_number: aldermans name}
    start_date :: ISO 8601 Date Time
    stop_date :: ISO 8601 Date Time
    input :: Map containing the params passed into the url.
